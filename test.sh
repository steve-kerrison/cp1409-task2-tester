#!/bin/bash
# This program does nothing except echo some output to stdout and stderr
# It was used for testing the tester...
echo "T: 0, 0, R
T: 1, 0, R
S: 0, 2, 0
T: 1, 1, W
T: 2, 1, R
T: 3, 1, R
T: 4, 1, R
S: 1, 3, 1
T: 5, -, I
T: 6, 2, R
S: 6, 1, 0
T: 7, 3, R
T: 8, 3, R
T: 9, 3, R
T: 10, 3, R
T: 11, 3, R
S: 3, 5, 0
T: 10, 4, W
T: 11, 4, W
T: 12, 4, R
T: 13, 4, R
T: 14, 4, R
S: 4, 3, 2
A: 5, 14, 1, 2.80, 0.60, 2.00"

>&2 echo "Number of processes: 5
Total time / running time / idle time: 14 / 13 / 1
Avg run time / avg wait / longest wait: 2.80 / 0.60 / 2.00"