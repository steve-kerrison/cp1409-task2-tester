#!/bin/bash
#
# A simple test script for program2_cpu_scheduler.sh
#
# Usage: ./program2_test.sh <test_cpu_scheduler> [<input_vectors> <expected_output>]
#
# Example: ./program2_test.sh steve/cp1409/program2_cpu_scheduler.sh
#      or:  ./program2_test.sh ./another_scheduler.sh tests/input.csv tests/verification.csv
#
# This script will run the specified scheduler script and then provide it with test input.
# The input will be from a simple listing shown below in INPUT_VECTORS, otherwise it will
# load them from the specivied <input_vectors> file for more elaborate tests.
#
# The output of the schedule will be checked against either OUTPUT_VECTORS, or if external
# input vectors were provided, it will check against a provided <expected_output.csv>,
# wherein standard output is provided first, then a line with `===`, followed by standard
# error.

# Argument check - no proper error checking, just a count-check and assume errors
# that emerge will be obvious to fix
if [ $# -eq 1 ]; then
    echo "Running using basic internal tests / verification"
    # Same as builtin_input.csv
    INPUT_VECTORS=$(cat <<EOT
0, 2
1, 3
6, 1
7, 5
10, 3

EOT
)
    # Same as builtin_verification.csv before the '===' line
    EXPECTED_STDOUT="T: 0, 0, R
T: 1, 0, R
S: 0, 2, 0
T: 1, 1, W
T: 2, 1, R
T: 3, 1, R
T: 4, 1, R
S: 1, 3, 1
T: 5, -, I
T: 6, 2, R
S: 2, 1, 0
T: 7, 3, R
T: 8, 3, R
T: 9, 3, R
T: 10, 3, R
T: 11, 3, R
S: 3, 5, 0
T: 10, 4, W
T: 11, 4, W
T: 12, 4, R
T: 13, 4, R
T: 14, 4, R
S: 4, 3, 2
A: 5, 15, 1, 2.80, .60, 2"

    # Same as builtin_verification.csv after the ''==='' line.
    EXPECTED_STDERR="Number of processes: 5
Total time / running time / idle time: 15 / 14 / 1
Avg run time / avg wait / longest wait: 2.80 / .60 / 2
" # Yes, this needs a newline -_-

elif [ $# -eq 3 ]; then
    echo "Running using external tests from '$2' and result vectors from '$3'"
        # Exit early on any errors here to avoid confusion
    set -e
    # Load the input vectors into a variable
    INPUT_VECTORS="$(cat "$2")"
    # Now iterate over the verification file, first appending to EXPECTED_STDOUT
    TO_ERR=0
    EXPECTED_STDOUT=""
    EXPECTED_STDERR=""
    while IFS= read -r LINE; do
        if [ "$LINE" == "===" ]; then
            # Once we've seen the line `===`, start appending to EXPECTED_STDERR instead
            TO_ERR=1
            continue
        elif [ $TO_ERR -eq 0 ]; then
            EXPECTED_STDOUT="$EXPECTED_STDOUT$LINE\n"
        else
            EXPECTED_STDERR="$EXPECTED_STDERR$LINE\n"
        fi
    done < "$3"
    # Relax error checking - don't exit automatically any more
    set +e
else
    # Wrong combination arguments
    echo "Usage: $0 <test_cpu_scheduler> [<input_vectors> <expected_output>]"
    exit 1
fi

# Test script exists and is executable
test -x $1 || { echo "Script '$1' not found or not executable"; exit 1; }

# Run the program twice, because it should be deterministic anyway
# at this level of simplicity
ACTUAL_STDOUT="$(echo "$INPUT_VECTORS" | $1 2>/dev/null)"
ACTUAL_STDERR="$(echo "$INPUT_VECTORS" | $1 2>&1 >/dev/null)"

ERRORS=0
echo "Comparing expected vs actual standard output (with line numbers added)..."
diff --color=always <(echo -en "$EXPECTED_STDOUT" | nl) <(echo -e "$ACTUAL_STDOUT" | nl)
STDOUT_RESULT=$?
if [ $STDOUT_RESULT -eq 0 ]; then
    echo "
    None found. Great!
"
else
    ((ERRORS++))
    echo "
*** Differences were found, check the above diff output carefully!
"
fi

echo "Comparing expected vs actual standard error..."
diff --color=always <(echo -en "$EXPECTED_STDERR") <(echo -e "$ACTUAL_STDERR")
STDERR_RESULT=$?
if [ $STDERR_RESULT -eq 0 ]; then
    echo "
    None found. Great!
"
else
    ((ERRORS++))
    echo "
*** Differences were found, check the above diff output carefully!
"
fi

if [ $ERRORS -ne 0 ]; then
    echo "Not all tests passed. Exiting with error"
    exit 1
fi

echo "Looks good to me, but this test might not cover all cases! Try expanding the tests."
# Program will exit 0 by default