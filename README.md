# CP1409-task2-tester

A script that can run a student's task schedule tracer with a given set of inputs, and check for valid output.

It includes a default input/output set, but can also load other input/output vectors from file.

The files `builtin_input.csv` and `builtin_verification.csv` are just copies of the default input/outputs
so you can see how to format external tests (if you want).

See the script itself (`program2_simple_test.sh`) for more information.